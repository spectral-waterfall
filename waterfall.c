#include <fcntl.h>
#include <fftw3.h>
#include <getopt.h>
#include <glib.h>
#include <math.h>
#include <gdk/gdkkeysyms.h>
#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define SAMPLE_RATE 44100
#define N_SLICES 64
#define SAMPLE_SIZE 2

struct waterfall_context {
	GtkDrawingArea *drawingarea;
	GtkRuler *frequency_ruler;
	GtkRuler *time_ruler;
	GdkPixbuf *original; /* A 1-to-1 map from spectral data to pixels. */
	GdkPixbuf *resized; /* A backing store for the scaled spectrogram. */
	struct {
		GdkPixbuf *part1; /* Slices from zero to the slice cursor. */
		GdkPixbuf *part2; /* Slices from the slice cursor to the end. */
		int part1_size;
		int part2_size;
	} subpixbufs;
	struct {
		/* The size of the spectrogram drawing area. */
		int width;
		int height;
	} size;
	struct {
		int f_low;
		int f_high;
		int noise_floor;
		double sensitivity;
		double hertz_scale;
	} zoom;
	char *buf;
	size_t buf_size;
	size_t buf_index;
	size_t integration_samples;
	int slice;
	int n_slices;
	struct {
		double *samples;
		fftw_complex *spectrum;
		fftw_plan p;
	} fft;
};

void invalidate_spectrogram(struct waterfall_context *waterfall);

void waterfall_open_output(GtkFileChooser *chooser, gpointer user_data)
{
	char *filename = gtk_file_chooser_get_filename(chooser);
	printf("Open %s\n", filename);
	g_free(filename);

	gtk_widget_hide(GTK_WIDGET(chooser));
}

void waterfall_expose_spectrogram(GtkWidget *widget,
				  GdkEventExpose *event,
				  gpointer user_data)
{
	GtkDrawingArea *spectrogram_drawingarea = GTK_DRAWING_AREA(widget);
	struct waterfall_context *waterfall = user_data;
	cairo_t *cr;

	if (!waterfall->resized) {
		double part1_size, part2_size;
		int resolution = waterfall->zoom.f_high - waterfall->zoom.f_low + 1;
		double scale_x = (double) waterfall->size.width / waterfall->n_slices;
		double scale_y = (double) waterfall->size.height / resolution;
		double offset_y = -waterfall->zoom.f_low * scale_y;
		int row, rowstride, channels;
		guchar *pixels;

		part1_size = (double) waterfall->subpixbufs.part1_size / waterfall->n_slices * waterfall->size.width;
		part2_size = waterfall->size.width - (int) part1_size;

		/* Create a new backing store, then render into it. */
		waterfall->resized = gdk_pixbuf_new(GDK_COLORSPACE_RGB, FALSE, 8, waterfall->size.width, waterfall->size.height);
		if (waterfall->subpixbufs.part2) {
			/* Render the oldest slices first. */
			gdk_pixbuf_scale(waterfall->subpixbufs.part2, waterfall->resized,
					 0, 0, (int) part2_size, waterfall->size.height,
					 0, offset_y, scale_x, scale_y,
					 GDK_INTERP_NEAREST);
		}
		if (waterfall->subpixbufs.part1) {
			/* Then render the more recent slices. */
			gdk_pixbuf_scale(waterfall->subpixbufs.part1, waterfall->resized,
					 (int) part2_size, 0, (int) part1_size, waterfall->size.height,
					 part2_size, offset_y, scale_x, scale_y,
					 GDK_INTERP_NEAREST);
		}
		pixels = gdk_pixbuf_get_pixels(waterfall->resized);
		rowstride = gdk_pixbuf_get_rowstride(waterfall->resized);
		channels = gdk_pixbuf_get_n_channels(waterfall->resized);
		/* Draw a narrow marker to represent the border between part1 and part2. */
		for (row = 0; row < waterfall->size.height; row++) {
			pixels[row*rowstride + ((int) part2_size % waterfall->size.width) * channels + 1] = 192;
		}
	}

	cr = gdk_cairo_create(spectrogram_drawingarea->widget.window);

	gdk_cairo_set_source_pixbuf(cr, waterfall->resized, 0, 0);
	cairo_paint(cr);

	cairo_destroy(cr);
}

void waterfall_configure_spectrogram(GtkWidget *widget,
				     GdkEventConfigure *event,
				     gpointer user_data)
{
	struct waterfall_context *waterfall = user_data;

	printf("configure %d %d,%d+%d,%d send_event=%d on window %p\n",
	       event->type, event->x, event->y, event->width, event->height, event->send_event, event->window);

	waterfall->size.width = event->width;
	waterfall->size.height = event->height;

	invalidate_spectrogram(waterfall);
}

gboolean waterfall_key_press(GtkWidget *widget,
			     GdkEventKey *event,
			     gpointer user_data)
{
	struct waterfall_context *waterfall = user_data;
	int resolution = waterfall->zoom.f_high - waterfall->zoom.f_low + 1;
	int headroom = waterfall->integration_samples/2 - waterfall->zoom.f_high;
	gboolean zoom_changed = FALSE;

	printf("key press\n");
	switch (event->keyval) {
	case GDK_KEY_Up:
		printf("up\n");
		waterfall->zoom.f_low -= MIN(waterfall->zoom.f_low, resolution / 2);
		waterfall->zoom.f_high -= MIN(waterfall->zoom.f_low, resolution / 2);
		zoom_changed = TRUE;
		break;
	case GDK_KEY_Down:
		printf("down\n");
		waterfall->zoom.f_low += MIN(headroom, resolution / 2);
		waterfall->zoom.f_high += MIN(headroom, resolution / 2);
		zoom_changed = TRUE;
		break;
	case GDK_KEY_plus:
		printf("plus\n");
		if (resolution / 4 > 1) {
			waterfall->zoom.f_low += resolution / 4;
			waterfall->zoom.f_high -= resolution / 4;
			zoom_changed = TRUE;
		}
		break;
	case GDK_KEY_minus:
		printf("minus\n");
		waterfall->zoom.f_low -= MIN(waterfall->zoom.f_low, resolution / 4);
		waterfall->zoom.f_high += MIN(headroom, resolution / 4);
		zoom_changed = TRUE;
		break;
	case GDK_KEY_bracketleft:
		printf("[\n");
		waterfall->zoom.noise_floor--;
		break;
	case GDK_KEY_bracketright:
		printf("]\n");
		waterfall->zoom.noise_floor++;
		break;
	case GDK_KEY_braceleft:
		printf("{\n");
		waterfall->zoom.sensitivity /= 1.5;
		break;
	case GDK_KEY_braceright:
		printf("}\n");
		waterfall->zoom.sensitivity *= 1.5;
		break;
	}

	if (zoom_changed) {
		invalidate_spectrogram(waterfall);
	}

	return FALSE;
}

void connect_signal(GtkBuilder *builder,
		    GObject *object,
		    const gchar *signal_name,
		    const gchar *handler_name,
		    GObject *connect_object,
		    GConnectFlags flags,
		    gpointer user_data)
{
	static struct {
		char const *name;
		GCallback fn;
	} handlers[] = {
		{ "gtk_main_quit", G_CALLBACK(&gtk_main_quit) },
		{ "gtk_widget_hide_on_delete", G_CALLBACK(&gtk_widget_hide_on_delete) },
		{ "gtk_widget_show", G_CALLBACK(&gtk_widget_show) },
		{ "waterfall_open_output", G_CALLBACK(&waterfall_open_output) },
		{ NULL, NULL }
	};
	int i;

	for (i = 0; handlers[i].name; i++) {
		if (!strcmp(handlers[i].name, handler_name)) {
			GCallback handler = handlers[i].fn;
			g_signal_connect_object(object, signal_name, handler,
						connect_object, flags);
			return;
		}
	}

	/* TODO: Connect to some error-spewing function? */
}

void invalidate_spectrogram(struct waterfall_context *waterfall)
{
	GdkRegion *visible_region;

	/* Just invalidate everything. */
	if (waterfall->resized) {
		g_object_unref(G_OBJECT(waterfall->resized));
		waterfall->resized = NULL;
	}
	visible_region = gdk_drawable_get_visible_region(waterfall->drawingarea->widget.window);
	gdk_window_invalidate_region(waterfall->drawingarea->widget.window,
				     visible_region,
				     FALSE);
	gdk_region_destroy(visible_region);

	gtk_ruler_set_range(waterfall->frequency_ruler,
			    waterfall->zoom.f_low * waterfall->zoom.hertz_scale,
			    waterfall->zoom.f_high * waterfall->zoom.hertz_scale,
			    waterfall->zoom.f_high * waterfall->zoom.hertz_scale,
			    (waterfall->zoom.f_high - waterfall->zoom.f_low) * waterfall->zoom.hertz_scale);
}

void split_pixbuf(struct waterfall_context *waterfall)
{
	int max_resolution = waterfall->integration_samples/2 + 1;

	if (waterfall->subpixbufs.part1) {
		g_object_unref(G_OBJECT(waterfall->subpixbufs.part1));
	}
	if (waterfall->subpixbufs.part2) {
		g_object_unref(G_OBJECT(waterfall->subpixbufs.part2));
	}

	waterfall->subpixbufs.part1_size = waterfall->slice % waterfall->n_slices;
	waterfall->subpixbufs.part2_size = waterfall->n_slices - waterfall->subpixbufs.part1_size;
	if (waterfall->subpixbufs.part1_size) {
		waterfall->subpixbufs.part1 = gdk_pixbuf_new_subpixbuf(waterfall->original,
								       0, 0,
								       waterfall->subpixbufs.part1_size, max_resolution);
	} else {
		waterfall->subpixbufs.part1 = NULL;
	}
	if (waterfall->subpixbufs.part2_size) {
		waterfall->subpixbufs.part2 = gdk_pixbuf_new_subpixbuf(waterfall->original,
								       waterfall->subpixbufs.part1_size, 0,
								       waterfall->subpixbufs.part2_size, max_resolution);
	} else {
		waterfall->subpixbufs.part2 = NULL;
	}
}

gboolean waterfall_input(GIOChannel *source,
			 GIOCondition condition,
			 gpointer userdata)
{
	struct waterfall_context *waterfall = userdata;
	double normalization = log10(waterfall->integration_samples * 32768);
	gsize n;
	int batch_size = 0;
	GError *err = NULL;

	switch (g_io_channel_read_chars(source,
					waterfall->buf,
					waterfall->buf_size - waterfall->buf_index,
					&n,
					&err)) {
	case G_IO_STATUS_NORMAL:
		waterfall->buf_index += n;
		break;
	case G_IO_STATUS_AGAIN:
		printf("Try again later\n");
		g_error_free(err);
		return TRUE;
	case G_IO_STATUS_EOF:
		printf("End of input\n");
		exit(0);
		break;
	case G_IO_STATUS_ERROR:
		printf("Error - %s\n", err->message);
		exit(1);
		break;
	default:
		exit(1);
		break;
	}

	while (waterfall->buf_index >= waterfall->integration_samples*SAMPLE_SIZE) {
		int i, row, rowstride, channels, modslice;
		int resolution = waterfall->integration_samples/2 + 1;
		guchar *pixels;

		/* Analyze the spectrum. */
		printf("Integrate %d %zu %d (%d)\n", batch_size, waterfall->buf_index,
		       waterfall->slice, waterfall->slice % waterfall->n_slices);
		for (i = 0; i < waterfall->integration_samples; i++) {
			int16_t x;
			memcpy(&x, waterfall->buf + i*SAMPLE_SIZE, sizeof (x));
			waterfall->fft.samples[i] = x;
		}
		fftw_execute(waterfall->fft.p);

		/* Draw a new slice. */
		modslice = waterfall->slice % waterfall->n_slices;
		pixels = gdk_pixbuf_get_pixels(waterfall->original);
		rowstride = gdk_pixbuf_get_rowstride(waterfall->original);
		channels = gdk_pixbuf_get_n_channels(waterfall->original);
		for (row = 0; row < resolution; row++) {
			double signal_i = waterfall->fft.spectrum[row][0], signal_q = waterfall->fft.spectrum[row][1];
			/* XXX Give invsqrt a chance to happen. */
			double power = -log10(1.0 / hypot(signal_i, signal_q)) - normalization;
			double z = (power + waterfall->zoom.noise_floor) * waterfall->zoom.sensitivity;
			if (row > 50 && row < 60) {
				printf("Power = %g (%g)\n", power, z);
			}
			/* Drawing to the original-size pixbuf, resampling happens in the expose event. */
			pixels[row*rowstride + modslice * channels + 0] = z >= 256 ? MIN(z - 256, 255) : 0;
			pixels[row*rowstride + modslice * channels + 1] = z >= 256 ? MAX(511 - z, 0) : MAX(z, 0);
			pixels[row*rowstride + modslice * channels + 2] = z < 256 ? MIN(255 - z, 255) : 0;
		}
		waterfall->slice++;

		/* Consume input. */
		memmove(waterfall->buf,
			waterfall->buf + waterfall->integration_samples*SAMPLE_SIZE,
			waterfall->buf_size - waterfall->integration_samples*SAMPLE_SIZE);
		waterfall->buf_index -= waterfall->integration_samples*SAMPLE_SIZE;
		batch_size++;
	}

	/* Scroll the waterfall. */
	split_pixbuf(waterfall);
	invalidate_spectrogram(waterfall);

	if (batch_size > 2) {
		printf("I can't keep up (%d)\n", batch_size);
	}

	if (err) {
		g_error_free(err);
	}

	return TRUE;
}

int main(int argc, char *argv[])
{
	GtkBuilder *gtk_builder;
	GtkWindow *waterfall_window;
	GIOChannel *stdin_channel;
	int stdin_flags;
	GError *err = NULL;
	struct waterfall_context waterfall = {
		.buf_index = 0,
		.slice = 0,
		.zoom = {
			.f_low = 0,
			.noise_floor = 5,
			.sensitivity = 200,
		},
	};

	waterfall.integration_samples = SAMPLE_RATE/10;
	waterfall.n_slices = N_SLICES;

	while (1) {
		int c = getopt(argc, argv, "H:t:");
		if (c == -1) {
			break;
		}

		switch (c) {
		case 'H':
			waterfall.n_slices = atoi(optarg);
			break;
		case 't':
			waterfall.integration_samples = atof(optarg) * SAMPLE_RATE;
			break;
		default:
			fprintf(stderr, "%s: '%c': Invalid option\n", argv[0], c);
			break;
		}
	}

	waterfall.zoom.f_high = waterfall.integration_samples/2;
	waterfall.zoom.hertz_scale = 21.050 / waterfall.zoom.f_high;

	gtk_init(&argc, &argv);

	gtk_builder = gtk_builder_new();
	gtk_builder_add_from_file(gtk_builder, "waterfall.glade", NULL);
	gtk_builder_connect_signals_full(gtk_builder, &connect_signal, NULL);

	/* Give signal handlers a means of accessing gtk_builder. */
	g_object_set(gtk_builder_get_object(gtk_builder,
					    "waterfall_window"),
		     "user-data", gtk_builder,
		     NULL);

	waterfall_window = GTK_WINDOW(gtk_builder_get_object(gtk_builder, "waterfall_window"));
	waterfall.drawingarea = GTK_DRAWING_AREA(gtk_builder_get_object(gtk_builder, "image_detail_drawingarea"));
	waterfall.frequency_ruler = GTK_RULER(gtk_builder_get_object(gtk_builder, "frequency_ruler"));
	waterfall.time_ruler = GTK_RULER(gtk_builder_get_object(gtk_builder, "time_ruler"));

	gtk_ruler_set_range(waterfall.time_ruler,
			    N_SLICES * waterfall.integration_samples / 44100.0,
			    0,
			    N_SLICES * waterfall.integration_samples / 44100.0,
			    N_SLICES * waterfall.integration_samples / 44100.0);

	g_signal_connect(G_OBJECT(waterfall.drawingarea), "expose-event",
			 G_CALLBACK(&waterfall_expose_spectrogram), &waterfall);
	g_signal_connect(G_OBJECT(waterfall.drawingarea), "configure-event",
			 G_CALLBACK(&waterfall_configure_spectrogram), &waterfall);
	g_signal_connect(G_OBJECT(waterfall_window), "key-press-event",
			 G_CALLBACK(&waterfall_key_press), &waterfall);

	/* Don't block on stdin. */
	stdin_flags = fcntl(0, F_GETFL, 0);
	fcntl(0, F_SETFL, stdin_flags | O_NONBLOCK);

	stdin_channel = g_io_channel_unix_new(0);
	g_io_channel_set_encoding(stdin_channel, NULL, &err);
	g_io_channel_set_buffer_size(stdin_channel, waterfall.integration_samples * SAMPLE_SIZE);
	waterfall.buf = malloc(waterfall.integration_samples * SAMPLE_SIZE);
	waterfall.buf_size = waterfall.integration_samples * SAMPLE_SIZE;
	g_io_add_watch(stdin_channel, G_IO_IN, &waterfall_input, &waterfall);

	waterfall.original = gdk_pixbuf_new(GDK_COLORSPACE_RGB, FALSE, 8,
					    waterfall.n_slices, waterfall.integration_samples/2 + 1);
	split_pixbuf(&waterfall);

	waterfall.fft.samples = fftw_malloc(sizeof (*waterfall.fft.samples) * waterfall.integration_samples);
	waterfall.fft.spectrum = fftw_malloc(sizeof (*waterfall.fft.spectrum) * (waterfall.integration_samples/2 + 1));
	waterfall.fft.p = fftw_plan_dft_r2c_1d(waterfall.integration_samples,
					       waterfall.fft.samples, waterfall.fft.spectrum,
					       FFTW_ESTIMATE);

	gtk_widget_show(GTK_WIDGET(waterfall_window));

	invalidate_spectrogram(&waterfall);

	gtk_main();

	fftw_destroy_plan(waterfall.fft.p);
	fftw_free(waterfall.fft.spectrum);
	fftw_free(waterfall.fft.samples);
}
